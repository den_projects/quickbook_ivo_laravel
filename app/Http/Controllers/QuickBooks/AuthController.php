<?php

namespace App\Http\Controllers\QuickBooks;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Services\QuickBooks\AuthService as QBAuth;

class AuthController
{
    /**
     * Handle QuickBooks Callback
     */
    public function QBAuthCallback(Request $request)
    {
        try{
            // save to DB company QBooks access token data
            $company = Company::findOrFail(auth()->user()->company_id);
            (new QBAuth($company))->exchangeAccessTokenByCode($request->get('code'), $request->get('realmId'));

            return redirect()->route('integration.qb')->with('success', 'Authentication successful');
        } catch(\Exception $e){
            return redirect()->route('integration')->with('error', $e->getMessage());
        }
    }
}

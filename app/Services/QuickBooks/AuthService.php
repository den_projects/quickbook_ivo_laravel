<?php

namespace App\Services\QuickBooks;

use App\Models\Company;
use QuickBooksOnline\API\DataService\DataService;

/** QuickBooks auth API
 * Class QBooksService
 * @package App\Services
 */
class AuthService
{
    private $instance;
    private $company;

    public function __construct(Company $company, $fail_if_token_expired=false)
    {
        $this->company = $company;
        // create QBooks user relation if not exists
        if(!$company->quickBooks){
            $company->quickBooks()->create([]);
        }
        if(!$this->checkCredentials()){
            throw new \Exception('API credentials required to connect QuickBooks API');
        }
        $this->instance = $this->getInstance();

        if($fail_if_token_expired && !$this->isValidAccessToken()){
            throw new \Exception('API tokens are expired. Please connect to your QuickBooks account.');
        }
    }

    public function getInstance()
    {
        if(!$this->instance || !$this->isValidAccessToken()){
            $this->instance = $this->initSDK();
        }
        return $this->instance;
    }

    /**
     * Initialize QuickBooks Data Service
     * @return DataService
     */
    protected function initSDK()
    {
        $sdk = DataService::Configure(
                array_merge($this->getDefaultConfig(),  [
                        'accessTokenKey'  => $this->company->quickBooks->token,
                        'QBORealmID'      => $this->company->quickBooks->real_id,
                    ]
                )
            );

        #1 Access by valid token
        if($this->isValidAccessToken()){
            return $sdk;
        }
        #2 valid refresh token -> renew access token
        if($this->isValidRefreshToken()) {

            $newOAuth2AccessToken = $sdk->getOAuth2LoginHelper()->refreshAccessTokenWithRefreshToken($this->company->quickBooks->refresh_token);
            $sdk->updateOAuth2Token($newOAuth2AccessToken);
            $sdk->throwExceptionOnError(true);
            $this->company->quickBooks->fillOAuthTokens($newOAuth2AccessToken)->save();
            $this->company->refresh();

            return $sdk;
        }
        #3 have no valid tokens -> delete tokens from DB -> Connect to QBooks for link account
        $this->company->quickBooks()->update(['token' => null, 'refresh_token' => null]);
        return DataService::Configure($this->getDefaultConfig());
    }

    // @return the link to QBooks authorization window
    public function getAuthURL()
    {
        $OAuthHelper = $this->getInstance()->getOAuth2LoginHelper();
        return $OAuthHelper->getAuthorizationCodeURL();
    }

    /** Update access token in Qbooks repository and returns OAuth2AccessToken object
     * @param $code
     * @param $realm_id
     * @return \QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2AccessToken
     */
    public function exchangeAccessTokenByCode($code, $realm_id)
    {
        $accessToken = $this->getInstance()->getOAuth2LoginHelper()->exchangeAuthorizationCodeForToken($code, $realm_id);
        $this->getInstance()->updateOAuth2Token($accessToken);
        $this->company->quickBooks->fillOAuthTokens($accessToken, $realm_id)->save();
        $this->company->refresh();
        return $accessToken;
    }

    protected function getDefaultConfig(){
        return [
            'auth_mode' => config('qbooks.default.auth_mode'),
            'ClientID' => config('qbooks.default.client_id'),
            'ClientSecret' => config('qbooks.default.client_secret'),
            'RedirectURI' => route('qb.auth.callback'),
            'scope' => config('qbooks.default.scope'),
            'baseUrl' => config('qbooks.default.base_url')
        ];
    }

    /** Check if quickbook credentials are not empty
     * @return array|bool
     */
    public function checkCredentials()
    {
        return empty(env('QUICKBOOKS_CLIENT_ID')) || empty(env('QUICKBOOKS_CLIENT_SECRET'))
            ? false
            : true;
    }

    /** Get actual access token OF false if need to link company
     * @return bool|\QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2AccessToken
     */
    public function getAccessToken()
    {
         return $this->isValidAccessToken() ? $this->company->quickBooks->token : false;
    }

    // check is access token expired
    public function isValidAccessToken():bool
    {
        return $this->company->quickBooks->isValidToken();
    }

    // check is refresh token expired
    public function isValidRefreshToken():bool
    {
        return $this->company->quickBooks->isValidRefreshToken();
    }

    public function getCompany()
    {
        return $this->company;
    }

    /**  Find company by ID received from QuickBooks API
     * @param $qb_id - QuickBooks 'real_id' key
     */
    /*public function setCompanyByQBId($qb_id):void
    {
        $this->company = Company::active()->whereHas('quickBooks', function($q) use ($qb_id){
            return $q->where('real_id', $qb_id);
        })->first();
    }*/
}

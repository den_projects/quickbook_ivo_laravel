<?php
/* Configuration file for QuickBooks service */
return [
    'default' => [
        'auth_mode'     => 'oauth2',
        'base_url'      => config('app.env') === 'production' ? 'production' : 'development',
        'client_id'     => env('QUICKBOOKS_CLIENT_ID'),
        'client_secret' => env('QUICKBOOKS_CLIENT_SECRET'),
        'scope'         => 'com.intuit.quickbooks.accounting',
        'auth_request_url' => 'https://appcenter.intuit.com/connect/oauth2'
    ]
];

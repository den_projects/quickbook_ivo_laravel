<?php

namespace App\Services\QuickBooks;


use App\Models\Client;
use Illuminate\Support\Facades\Log;
use QuickBooksOnline\API\Facades\Customer as QBCustomer;

/** QuickBooks Client API
 * Class QBooksService
 * @package App\Services
 */
class CustomerService
{
    private $sdk;
    private $company_id;
    private $loop_chunk = 100; // size of data chunk on loop through database or request

    public function __construct(AuthService $aus)
    {
        $this->sdk = $aus->getInstance();
        $this->company_id = $aus->getCompany()->id;
    }

    /** Batch import customers from QuickBooks to IVO CRM
     * @return array
     */
    public function batchImport($override=false)
    {
        //return $this->sdk->Query("SELECT * FROM Customer WHERE 0");
        $from = 1;
        $total_cnt = $imported_cnt = 0;
        do{
            $customers = $this->sdk->findAll('Customer', $from, $this->loop_chunk);
            if(is_array($customers) && count($customers) > 0){
                foreach($customers as $data){
                    if($this->importCustomerToIVOCRM($data, $override)){
                        $imported_cnt++;
                    }
                    $total_cnt++;
                }
            }
            $from += $this->loop_chunk;

        } while($customers && (count($customers) > 0));

        return [
            'total' => $total_cnt,
            'imported' => $imported_cnt,
        ];
    }

    /** Compare customer by email OR by full name if email empty
     * @param $data
     * @param $override - override existing customer data
     * @return bool
     */
    protected function importCustomerToIVOCRM($data, $override=false):bool
    {
        if(empty($data) || (!isset($data->PrimaryEmailAddr->Address) && !isset($data->GivenName) && !isset($data->FamilyName))){
            return false;
        }

        // find client by QB id OR e-mail OR by name in local DB
        $ivo_customer = Client::where('company_id', $this->company_id)->where(function($qw) use ($data){
            return $qw->where(function($q1) use ($data){
                // search by QBooks internal Id
                return $q1->whereNotNull('qbooks_id')->where('qbooks_id', $data->Id);
            })->orWhere(function($q2) use($data){
                // search by contact e-mail OR by name
                return $q2->when(isset($data->PrimaryEmailAddr->Address), function($q) use($data){
                    return $q->where('contact_email', $data->PrimaryEmailAddr->Address);
                })
                ->when(!isset($data->PrimaryEmailAddr->Address) && !empty($data->GivenName), function($q) use($data){
                    return $q->where(['first_name' => $data->GivenName, 'last_name' => $data->FamilyName]);
                });
            });
        })->first();

        // create customer if not exists
        if(!$ivo_customer){
            return
                Client::create([
                    'first_name' => $data->GivenName,
                    'last_name' => $data->FamilyName,
                    'qbooks_id' => $data->Id,
                    'contact_email' => $data->PrimaryEmailAddr->Address ?? '',
                    'company_id' => $this->company_id,
                    'contact_phone_1' => $data->PrimaryPhone->FreeFormNumber ?? '',
                    'contact_cell' => $data->Mobile->FreeFormNumber ?? '',
                    'contact_fax' => $data->Fax->FreeFormNumber ?? '',

                    'contact_name' => $data->GivenName.' '.$data->FamilyName,
                    'contact_address_1' => $data->BillAddr->Line1 ?? '',
                    'contact_address_2' => $data->BillAddr->Line2 ?? '',
                    'contact_city' => $data->BillAddr->City ?? '',
                    'contact_state' => $data->BillAddr->CountrySubDivisionCode ?? '',
                    'contact_zip' => $data->BillAddr->PostalCode ?? '',
                    'contact_country' => $data->BillAddr->Country ?? '',

                    'note' => $data->Notes ?? '',
                    'settings' => ["send_sms" => 1, "send_email" => 1]
                ])
                ? true
                : false;
        }
        // update customer data
        elseif($ivo_customer && $override){
            return $ivo_customer->update([
                'first_name' => $data->GivenName,
                'last_name' => $data->FamilyName,
                'qbooks_id' => $data->Id,
                'contact_email' => $data->PrimaryEmailAddr->Address ?? '',
                'contact_phone_1' => $data->PrimaryPhone->FreeFormNumber ?? '',
                'contact_cell' => $data->Mobile->FreeFormNumber ?? '',
                'contact_fax' => $data->Fax->FreeFormNumber ?? '',

                'contact_name' => $data->GivenName.' '.$data->FamilyName,
                'contact_address_1' => $data->BillAddr->Line1 ?? '',
                'contact_address_2' => $data->BillAddr->Line2 ?? '',
                'contact_city' => $data->BillAddr->City ?? '',
                'contact_state' => $data->BillAddr->CountrySubDivisionCode ?? '',
                'contact_zip' => $data->BillAddr->PostalCode ?? '',
                'contact_country' => $data->BillAddr->Country ?? '',

                'note' => $data->Notes ?? '',
            ])
            ? true
            : false;
        } elseif($ivo_customer && empty($ivo_customer->qbooks_id)){
            // link to QB id
            return $ivo_customer->update(['qbooks_id' => $data->Id]) ? true : false;
        }
        return false;
    }

    /** Batch export customers data from IVO CRM to QBooks account
     * @param bool $override
     * @return array
     */
    public function batchExport($override=false)
    {
        #1 get principal customers data from QB [qb_id, name, email]
        $qb_customers = collect([]);
        $ivo_customers = Client::active()->where('company_id', $this->company_id);
        $created_cnt = 0;
        $updated_cnt = 0;
        $from = 1;
        do{
            $customers = $this->sdk->findAll('Customer', $from, $this->loop_chunk);
            if(is_array($customers) && count($customers) > 0){

                foreach($customers as $data){
                    $qb_customers->push([
                        'id' => $data->Id,
                        'first_name' => $data->GivenName,
                        'last_name' => $data->FamilyName,
                        'email' => $data->PrimaryEmailAddr->Address ?? ''
                    ]);
                }
            }
            $from += $this->loop_chunk;
        } while($customers && (count($customers) > 0));

        #2 check existing customers and make corresponding create/update request
        if($ivo_customers->count() > 0){
            $ivo_customers->chunkById($this->loop_chunk, function($customers) use(&$created_cnt, &$updated_cnt, $qb_customers, $override){
                foreach($customers as $ivo_customer){

                    $matched_qb_customer = $qb_customers->filter(function ($item, $key) use ($ivo_customer){
                        return $ivo_customer->qbooks_id == $item['id']
                                || (!empty($ivo_customer->contact_email) && $ivo_customer->contact_email == $item['email'])
                                || (
                                    (!empty($ivo_customer->first_name) && $ivo_customer->first_name == $item['first_name'])
                                    &&
                                    ($ivo_customer->last_name == $item['last_name'])
                                   );
                    })->first();

                    // if QB customer exists
                    if($matched_qb_customer){
                        // update qb customer
                        if($override){

                            if($qb_customer_entities = $this->sdk->Query("SELECT * FROM Customer WHERE Id = '".$matched_qb_customer['id']."'")){
                                $qb_customer_obj = reset($qb_customer_entities);

                                if($this->sdk->Update(QBCustomer::update($qb_customer_obj, $this->prepareQBData($ivo_customer)))){
                                    $updated_cnt++;
                                }
                                if($error = $this->sdk->getLastError()){
                                    Log::error('Batch import[Update]: Error on update QuickBook customer '.$ivo_customer->full_name.'; Response: '.$error->getResponseBody());
                                }
                            }
                        }
                    }

                    // not exist qb customer -> lets create
                    else{
                        $qb_customer = QBCustomer::create($this->prepareQBData($ivo_customer));

                        if($added = $this->sdk->Add($qb_customer)){
                            // save QBooks customer ID to related customer from IVO
                            if(isset($added->Id)){
                                $ivo_customer->qbooks_id = $added->Id;
                                $ivo_customer->save();
                            }
                            $created_cnt++;
                        }
                        if($error = $this->sdk->getLastError()){
                            Log::error('Batch import[Create]: Error on create QuickBook customer '.$ivo_customer->full_name.';  Response: '.$error->getResponseBody());
                        }
                    }
                }
            });
        }

        return [
            'total' => $ivo_customers->count(),
            'created' => $created_cnt,
            'updated' => $updated_cnt
        ];
    }

    /** Prepare Qbooks customer data structure
     * @param Client $ivo_customer
     * @return mixed
     */
    protected function prepareQBData(Client $ivo_customer)
    {
        if(!$ivo_customer) return [];

        return [
            "GivenName" =>  $ivo_customer->first_name,
            "FamilyName" => $ivo_customer->last_name,
            "DisplayName" => $ivo_customer->full_name,
            "PrimaryPhone" => [
                "FreeFormNumber" => $ivo_customer->contact_phone_1 ?: $ivo_customer->contact_phone_2
            ],
            "PrimaryEmailAddr"=>  [
                "Address" => $ivo_customer->contact_email,
            ],
            "Mobile" => [
                "FreeFormNumber" => $ivo_customer->contact_cell
            ],
            "Fax" => [
                "FreeFormNumber" => $ivo_customer->contact_fax
            ],
            "BillAddr" => [
                "Line1" =>  $ivo_customer->contact_address_1,
                "City" =>  $ivo_customer->contact_city,
                "Country" =>  $ivo_customer->contact_country,
                "CountrySubDivisionCode" =>  $ivo_customer->contact_state,
                "PostalCode" =>  $ivo_customer->contact_zip
            ],
            "Notes" => $ivo_customer->note
        ];
    }

}
